package it.unibz.sipai.domain;

import it.unibz.sipai.util.GroupTransformer;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Stack;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.ElementCollection;
import javax.persistence.Embedded;
import javax.persistence.EntityManager;
import javax.persistence.EntityResult;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OrderBy;
import javax.persistence.Query;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.TypedQuery;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.GenericGenerator;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.json.RooJson;
import org.springframework.roo.addon.tostring.RooToString;
import org.springframework.transaction.annotation.Transactional;

import flexjson.JSON;
import flexjson.JSONSerializer;
import flexjson.transformer.DateTransformer;

@RooJavaBean
@RooToString
@SqlResultSetMapping(name = "ItemWithDistance", entities = @EntityResult(entityClass = Item.class), columns = @ColumnResult(name = "DISTANCE"))
@RooJson
@RooJpaActiveRecord(versionField = "", finders = { "findItemsByItemCategories", "findItemsByRemoteSystemInfo" })
public class Item {

	@Id
	@GenericGenerator(name = "generator", strategy = "increment", parameters = {})
	@GeneratedValue(generator = "generator")    
	@Column(name = "id")
	private Long id;

	private String name;
	
	@NotNull
	@Embedded
	private RemoteSystemInfo remoteSystemInfo;

	@Embedded
	private Location location;
	
	@ManyToOne
	private District district;

	@Column(name = "averagerating")
	private Double averageRating;

	private Boolean deleted;

	@ManyToMany(cascade = CascadeType.ALL)
	@OrderBy("id")
	private Set<ItemCategory> itemCategories = new HashSet<ItemCategory>();
	
	@ElementCollection
	private List<String> typesOfKitchen;
	
	private String accommodationCategory;
	
	@Column(columnDefinition = "TEXT")
	private String introduction;

	@Column(columnDefinition = "TEXT")
	private String description;

	private transient String website;

	private transient String phone;

	private transient String address;
	
	private transient String imageUrl;

	private transient String email;

	@DateTimeFormat(pattern = "dd/MM/yyyy")
	private transient Date fromDate;

	@DateTimeFormat(pattern = "dd/MM/yyyy")
	private transient Date toDate;

	private transient String gettingThere;

	private transient String organizer;

	private transient String startPoint;

	private transient String walkTime;

	private transient String highestPoint;

	private transient String altitudeDifference;

	private transient String mapGuide;

	private transient String difficultyRating;

	private transient Double distance;

	private transient Long bookmarkedItemId;

	private transient MusicTrack musicTrack;

	private transient List<ItemTag> itemTags;

	private transient Review myReview;

	private transient Group<Review> reviews;
	
	private transient String activeLearningInfo;
		
	private transient String suggestionSession;

	private transient Long parkingSlotsCount;
	
	private transient Long freeParkingSlotsCount;
	
	private transient Integer weather;
	
	private transient Double parkingOccupancy;
	
	private transient List<ContextFactor> contextFactors;
	
	@JSON(include = false)
	public RemoteSystemInfo getRemoteSystemInfo() {
		return remoteSystemInfo;
	}
	
	@JSON(include = false)
	public District getDistrict() {
		return district;
	}

	@JSON(include = true)
	public Set<ItemCategory> getItemCategories() {
		return itemCategories;
	}

	@JSON(include = false)
	public Boolean getDeleted() {
		return deleted;
	}

	public String getIntroduction() {
		return introduction;
	}

	public void setIntroduction(String introduction) {
		this.introduction = introduction;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public List<String> getTypesOfKitchen() {
		return typesOfKitchen;
	}

	public void setTypesOfKitchen(List<String> typesOfKitchen) {
		this.typesOfKitchen = typesOfKitchen;
	}

	public String getAccommodationCategory() {
		return accommodationCategory;
	}

	public void setAccommodationCategory(String accommodationCategory) {
		this.accommodationCategory = accommodationCategory;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public String getGettingThere() {
		return gettingThere;
	}

	public void setGettingThere(String gettingThere) {
		this.gettingThere = gettingThere;
	}

	public String getOrganizer() {
		return organizer;
	}

	public void setOrganizer(String organizer) {
		this.organizer = organizer;
	}

	public String getStartPoint() {
		return startPoint;
	}

	public void setStartPoint(String startPoint) {
		this.startPoint = startPoint;
	}

	public String getWalkTime() {
		return walkTime;
	}

	public void setWalkTime(String walkTime) {
		this.walkTime = walkTime;
	}

	public String getHighestPoint() {
		return highestPoint;
	}

	public void setHighestPoint(String highestPoint) {
		this.highestPoint = highestPoint;
	}

	public String getAltitudeDifference() {
		return altitudeDifference;
	}

	public void setAltitudeDifference(String altitudeDifference) {
		this.altitudeDifference = altitudeDifference;
	}

	public String getMapGuide() {
		return mapGuide;
	}

	public void setMapGuide(String mapGuide) {
		this.mapGuide = mapGuide;
	}

	public String getDifficultyRating() {
		return difficultyRating;
	}

	public void setDifficultyRating(String difficultyRating) {
		this.difficultyRating = difficultyRating;
	}

	public Double getDistance() {
		return distance;
	}

	public void setDistance(Double distance) {
		this.distance = distance;
	}

	public Long getBookmarkedItemId() {
		return bookmarkedItemId;
	}

	public void setBookmarkedItemId(Long bookmarkedItemId) {
		this.bookmarkedItemId = bookmarkedItemId;
	}

	public MusicTrack getMusicTrack() {
		return musicTrack;
	}

	public void setMusicTrack(MusicTrack musicTrack) {
		this.musicTrack = musicTrack;
	}

	public List<ItemTag> getItemTags() {
		return itemTags;
	}

	public void setItemTags(List<ItemTag> itemTags) {
		this.itemTags = itemTags;
	}

	public Review getMyReview() {
		return myReview;
	}

	public void setMyReview(Review myReview) {
		this.myReview = myReview;
	}

	public Group<Review> getReviews() {
		return reviews;
	}

	public void setReviews(Group<Review> reviews) {
		this.reviews = reviews;
	}
	
	public String getActiveLearningInfo() {
		return activeLearningInfo;
	}
	
	public void setActiveLearningInfo(String activeLearningInfo) {
		this.activeLearningInfo = activeLearningInfo;
	}
	
	public String getSuggestionSession() {
		return suggestionSession;
	}
	
	public void setSuggestionSession(String suggestionSession) {
		this.suggestionSession = suggestionSession;
	}
	
	public Long getParkingSlotsCount() {
		return parkingSlotsCount;
	}
	
	public void setParkingSlotsCount(Long parkingSlotsCount) {
		this.parkingSlotsCount = parkingSlotsCount;
	}
	
	public Long getFreeParkingSlotsCount() {
		return freeParkingSlotsCount;
	}
	
	public void setFreeParkingSlotsCount(Long freeParkingSlotsCount) {
		this.freeParkingSlotsCount = freeParkingSlotsCount;
	}
	
	public Integer getWeather() {
		return weather;
	}
	
	public void setWeather(Integer weather) {
		this.weather = weather;
	}
	
	public Double getParkingOccupancy() {
		return parkingOccupancy;
	}
	
	public void setParkingOccupancy(Double parkingOccupancy) {
		this.parkingOccupancy = parkingOccupancy;
	}
	
	public List<ContextFactor> getContextFactors() {
		return contextFactors;
	}

	public void setContextFactors(List<ContextFactor> contextFactors) {
		this.contextFactors = contextFactors;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((remoteSystemInfo == null) ? 0 : remoteSystemInfo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		Item other = (Item) obj;
		if (remoteSystemInfo == null) {
			if (other.remoteSystemInfo != null) return false;
		} else if (!remoteSystemInfo.equals(other.remoteSystemInfo)) return false;
		return true;
	}

	/**
	 * Extends <code>this</code> with <code>item</code>. Non-null values from <code>item</code> override values in <code>this</code>.
	 * @param item
	 */
	 public void extend(Item item) {
		 Method[] methods = this.getClass().getMethods();
		 for (Method fromMethod : methods) {
			 if (fromMethod.getDeclaringClass().equals(this.getClass()) && fromMethod.getName().startsWith("get")) {
				 String fromName = fromMethod.getName();
				 String toName = fromName.replace("get", "set");
				 try {
					 Method toMethod = this.getClass().getMethod(toName, fromMethod.getReturnType());
					 Object value = fromMethod.invoke(item, (Object[]) null);
					 if (value != null) {
						 toMethod.invoke(this, value);
					 }
				 } catch (Exception e) {
					 e.printStackTrace();
				 }
			 }
		 }
	 }

	 public String toJson() {
		 return new JSONSerializer().transform(new GroupTransformer(), Group.class).transform(new DateTransformer("dd/MM/yyyy"), Date.class).exclude("*.class").include("typesOfKitchen", "itemTags", "reviews", "contextFactors", "reviews.contextValues", "myReview.contextValues").exclude("itemTags.item", "itemTags.userEntity", "reviews.userEntity", "myReview.userEntity").serialize(this);
	 }

	 public static String toJsonArray(Collection<Item> collection) {
		 return new JSONSerializer().transform(new GroupTransformer(), Group.class).transform(new DateTransformer("dd/MM/yyyy"), Date.class).exclude("*.class", "introduction", "description", "accommodationCategory", "fromDate", "toDate", "gettingThere", "organizer", "startPoint", "walkTime", "highestPoint", "altitudeDifference", "mapGuide", "difficultyRating", "myReview", "musicTrack", "contextFactors").serialize(collection);
	 }
	 
	 @SuppressWarnings("unchecked")
	 public static List<Long> findUnreviewedTripAdvisorItemIdsByUserEntity(UserEntity userEntity) {
		 if (userEntity == null) throw new IllegalArgumentException("The userEntity argument is required");
		 StringBuilder query = new StringBuilder();
		 query.append("SELECT DISTINCT(i.item) FROM trip_advisor_item i LEFT OUTER JOIN (SELECT * from review WHERE user_entity = :username) r ON i.item = r.item WHERE r.id IS NULL ");
		 
		 EntityManager em = entityManager();
		 Query q = em.createNativeQuery(query.toString());
		 q.setParameter("username", userEntity.getUsername());
		 
		 List<Object> resultList = (List<Object>) q.getResultList();
		 List<Long> ids = new ArrayList<Long>();
		 for (Object result : resultList) {
			ids.add(Long.parseLong(result.toString()));
		 }
		 return ids;
	 }
	 
	 @SuppressWarnings("unchecked")
	 public static List<Long> findUnreviewedPopularTripAdvisorItemIdsByUserEntity(UserEntity userEntity) {
		 if (userEntity == null) throw new IllegalArgumentException("The userEntity argument is required");
		 StringBuilder query = new StringBuilder();
		 query.append("SELECT DISTINCT(i.item) FROM trip_advisor_item i LEFT OUTER JOIN (SELECT * from review WHERE user_entity = :username) r ON i.item = r.item WHERE r.id IS NULL AND i.popular = TRUE");
		 
		 EntityManager em = entityManager();
		 Query q = em.createNativeQuery(query.toString());
		 q.setParameter("username", userEntity.getUsername());
		 
		 List<Object> resultList = (List<Object>) q.getResultList();
		 List<Long> ids = new ArrayList<Long>();
		 for (Object result : resultList) {
			ids.add(Long.parseLong(result.toString()));
		 }
		 return ids;
	 }
	 
	 @SuppressWarnings("unchecked")
	 public static List<Long> findAllTripAdvisorItemIds() {
		 StringBuilder query = new StringBuilder();
		 query.append("SELECT DISTINCT(i.item) FROM trip_advisor_item i");
		 
		 EntityManager em = entityManager();
		 Query q = em.createNativeQuery(query.toString());
		 
		 List<Object> resultList = (List<Object>) q.getResultList();
		 List<Long> ids = new ArrayList<Long>();
		 for (Object result : resultList) {
			ids.add(Long.parseLong(result.toString()));
		 }
		 return ids;
	 }
	 
	 @SuppressWarnings("unchecked")
	 public static List<Long> findUnreviewedItemIdsByUserEntity(UserEntity userEntity, Set<ItemCategory> excludedItemCategories) {
		 if (userEntity == null) throw new IllegalArgumentException("The userEntity argument is required");
		 StringBuilder query = new StringBuilder("; WITH RECURSIVE item_macro_categories (id, top_key) AS (");
		 query.append("SELECT id, id FROM item_category WHERE parent_item_category IS NULL ");
		 query.append("UNION ALL ");
		 query.append("SELECT chdd.id, par.top_key FROM item_macro_categories par JOIN item_category chdd ON chdd.parent_item_category = par.id) ");
		 query.append("SELECT DISTINCT(i.id) FROM item i LEFT OUTER JOIN (SELECT * from review WHERE user_entity = :username) r ON i.id = r.item JOIN (SELECT item AS item_id, item_categories AS item_category_id, top_key AS top_category_id FROM item_macro_categories imc JOIN item_item_categories iic ON imc.id = iic.item_categories) AS ict ON i.id = ict.item_id WHERE r.id IS NULL ");
		 
		 boolean hasExcludedItemCategories = false;
		 if (excludedItemCategories != null && excludedItemCategories.size() > 0) {
			 hasExcludedItemCategories = true;
			 query.append("AND top_category_id NOT IN (:excludedcategoryids)");
		 }
		 
		 EntityManager em = entityManager();
		 Query q = em.createNativeQuery(query.toString());
		 q.setParameter("username", userEntity.getUsername());
		 if (hasExcludedItemCategories) {
			 Set<Long> categoryIds = new HashSet<Long>();
			 for (ItemCategory itemCategory : excludedItemCategories) {
				categoryIds.add(itemCategory.getId());
			}
			 q.setParameter("excludedcategoryids", categoryIds);
		 }
		 
		 List<Object> resultList = (List<Object>) q.getResultList();
		 List<Long> ids = new ArrayList<Long>();
		 for (Object result : resultList) {
			ids.add(Long.parseLong(result.toString()));
		 }
		 return ids;
	 }

	 @SuppressWarnings("unchecked")
	 public static List<Object[]> findItemIds(Location location, Set<ItemCategory> includedItemCategories, Set<ItemCategory> excludedItemCategories) {
		 StringBuilder query = new StringBuilder("; WITH RECURSIVE item_macro_categories (id, top_key) AS (");
		 query.append("SELECT id, id FROM item_category WHERE parent_item_category IS NULL ");
		 query.append("UNION ALL ");
		 query.append("SELECT chdd.id, par.top_key FROM item_macro_categories par JOIN item_category chdd ON chdd.parent_item_category = par.id) ");
		 query.append("SELECT id AS item_id, item_category_id, ST_Distance_Sphere(location, ST_GeomFromText('POINT(' || i.longitude || ' ' || i.latitude || ')', 4326)) / 1000 AS distance, top_category_id AS top_category, district ");
		 query.append("FROM (SELECT ST_GeomFromText('POINT(" + location.getLongitude() + " " + location.getLatitude() + ")', 4326) AS location) AS location, item AS i JOIN (SELECT item AS item_id, item_categories AS item_category_id, top_key AS top_category_id FROM item_macro_categories imc JOIN item_item_categories iic ON imc.id = iic.item_categories) AS ict ON (i.id = ict.item_id) ");
		 
		 boolean hasIncludedItemCategories = false;
		 if (includedItemCategories != null && includedItemCategories.size() > 0) {
			 hasIncludedItemCategories = true;
			 query.append("WHERE top_category_id IN (:includedcategoryids) ");
		 }
		 
		 boolean hasExcludedItemCategories = false;
		 if (excludedItemCategories != null && excludedItemCategories.size() > 0) {
			 hasExcludedItemCategories = true;
			 if (!hasIncludedItemCategories) {
				 query.append("WHERE ");
			 } else {
				 query.append("AND ");
			 }
			 query.append("top_category_id NOT IN (:excludedcategoryids)");
		 }
		 
		 EntityManager em = entityManager();
		 Query q = em.createNativeQuery(query.toString());

		 if (hasIncludedItemCategories) {
			 Set<Long> categoryIds = new HashSet<Long>();
			 for (ItemCategory itemCategory : includedItemCategories) {
				categoryIds.add(itemCategory.getId());
			}
			 q.setParameter("includedcategoryids", categoryIds);
		 }
		 if (hasExcludedItemCategories) {
			 Set<Long> categoryIds = new HashSet<Long>();
			 for (ItemCategory itemCategory : excludedItemCategories) {
				categoryIds.add(itemCategory.getId());
			}
			 q.setParameter("excludedcategoryids", categoryIds);
		 }

		 return q.getResultList();
	 }
	 
	 @Transactional
	 public static void markAllItemsDeleted() {
		 entityManager().createQuery("UPDATE Item SET deleted = true").executeUpdate();
	 }

	 @Transactional
	 public static void removeAllItemsDeleted() {
		 EntityManager em = entityManager();
		 em.createNativeQuery("DELETE FROM feedback f WHERE EXISTS (SELECT 1 FROM item i WHERE i.deleted AND i.id = f.item)").executeUpdate();
		 em.createNativeQuery("DELETE FROM transient_review_context_values trcv WHERE EXISTS (SELECT 1 FROM item i JOIN transient_review tr ON i.id = tr.item WHERE i.deleted AND trcv.transient_review = tr.id)").executeUpdate();
		 em.createNativeQuery("DELETE FROM transient_review tr WHERE EXISTS (SELECT 1 FROM item i WHERE i.deleted AND i.id = tr.item)").executeUpdate();
		 em.createNativeQuery("DELETE FROM trip_advisor_item ta WHERE EXISTS (SELECT 1 FROM item i WHERE i.deleted AND i.id = ta.item)").executeUpdate();
		 em.createNativeQuery("DELETE FROM item_types_of_kitchen itk WHERE EXISTS (SELECT 1 FROM item i WHERE i.deleted AND i.id = itk.item)").executeUpdate();
		 em.createNativeQuery("DELETE FROM item_tag it WHERE EXISTS (SELECT 1 FROM item i WHERE i.deleted AND i.id = it.item)").executeUpdate();
		 em.createNativeQuery("DELETE FROM review_context_values rcv WHERE EXISTS (SELECT 1 FROM item i JOIN review r ON i.id = r.item WHERE i.deleted AND rcv.review = r.id)").executeUpdate();
		 em.createNativeQuery("DELETE FROM review r WHERE EXISTS (SELECT 1 FROM item i WHERE i.deleted AND i.id = r.item)").executeUpdate();
		 em.createNativeQuery("DELETE FROM music2item_similarity m WHERE EXISTS (SELECT 1 FROM item i WHERE i.deleted AND i.id = m.item)").executeUpdate();
		 em.createNativeQuery("DELETE FROM bookmarked_item b WHERE EXISTS (SELECT 1 FROM item i WHERE i.deleted AND i.id = b.item)").executeUpdate();
		 em.createNativeQuery("DELETE FROM item_item_categories iic WHERE EXISTS (SELECT 1 FROM item i WHERE i.deleted AND iic.item = i.id)").executeUpdate();
		 em.createNativeQuery("DELETE FROM skipped_item s WHERE EXISTS (SELECT 1 FROM item i WHERE i.deleted AND i.id = s.item)").executeUpdate();
		 em.createNativeQuery("DELETE FROM item i WHERE i.deleted").executeUpdate();
	 }

	 public static TypedQuery<Item> findItemsByRemoteSystemNameAndRemoteSystemItemIdEqual(String remoteSystemName, String remoteSystemItemId) {
		 if (remoteSystemName == null || remoteSystemName.length() == 0) throw new IllegalArgumentException("The remoteSystemName argument is required");
		 if (remoteSystemItemId == null || remoteSystemItemId.length() == 0) throw new IllegalArgumentException("The remoteSystemItemId argument is required");
		 TypedQuery<Item> q = entityManager().createQuery("SELECT o FROM Item AS o WHERE o.remoteSystemInfo.remoteSystemName = :remoteSystemName AND o.remoteSystemInfo.remoteSystemItemId = :remoteSystemItemId", Item.class);
		 q.setParameter("remoteSystemName", remoteSystemName);
		 q.setParameter("remoteSystemItemId", remoteSystemItemId);
		 return q;
	 }
	 
	 public static TypedQuery<Item> findItemsByRemoteSystemItemIdEqual(String remoteSystemItemId) {
		 if (remoteSystemItemId == null || remoteSystemItemId.length() == 0) throw new IllegalArgumentException("The remoteSystemItemId argument is required");
		 TypedQuery<Item> q = entityManager().createQuery("SELECT o FROM Item AS o WHERE o.remoteSystemInfo.remoteSystemItemId = :remoteSystemItemId", Item.class);
		 q.setParameter("remoteSystemItemId", remoteSystemItemId);
		 return q;
	 }
	 
	 public static List<Item> findItemEntries(Location location, int radius, int firstResult, int maxResults, Sort sort, Set<ItemCategory> itemCategories) {
		 StringBuilder query = new StringBuilder("SELECT DISTINCT(i.*), ");
		 if (location != null) {
			 query.append("ST_Distance_Sphere(ST_GeomFromText('POINT(" + location.getLongitude() + " " + location.getLatitude() + ")', 4326), ST_GeomFromText('POINT(' || longitude || ' ' || latitude || ')', 4326)) / 1000 AS distance ");
			 query.append("FROM (SELECT ST_GeomFromText('POINT(" + location.getLongitude() + " " + location.getLatitude() + ")', 4326) AS location) AS location, item i ");
		 } else {
			 query.append("NULL AS distance ");
			 query.append("FROM item i ");
		 }
		
		 Set<Long> categoryIds = new HashSet<Long>();
		 if (itemCategories != null && !itemCategories.isEmpty()) {
			 Stack<ItemCategory> categories = new Stack<ItemCategory>();
			 categories.addAll(itemCategories);
			 while (!categories.isEmpty()) {
				 ItemCategory category = categories.pop();
				 if (!categoryIds.contains(category.getId())) {
					 categoryIds.add(category.getId());
					 categories.addAll(category.getChildItemCategories());
				 }
			 }
			 query.append("INNER JOIN item_item_categories iic ON iic.item = i.id INNER JOIN item_category ic ON ic.id = iic.item_categories WHERE ic.id IN (:categoryids) ");
		 } 
		 
		 if (radius >= 0 && location != null) {
			 if (categoryIds.isEmpty()) {
				 query.append("WHERE ");
			 } else {
				 query.append("AND ");
			 }
			 query.append("ST_Distance_Sphere(location, ST_GeomFromText('POINT(' || longitude || ' ' || latitude || ')', 4326)) <= :radius ");
		 }
		 
		 query.append("ORDER BY ");
		 query.append(sort.value);
		 if (sort == Sort.AVERAGE_RATING) {
			 query.append(" DESC NULLS LAST");
		 }
			 
		 EntityManager em = entityManager();
		 Query q = em.createNativeQuery(query.toString(), "ItemWithDistance");
		 if (location != null && radius >= 0) {
			 q.setParameter("radius", radius);
		 }
		 if (!categoryIds.isEmpty()) {
			 q.setParameter("categoryids", categoryIds);
		 }
		 
		 List<Object[]> r = (List<Object[]>) q.setFirstResult(firstResult).setMaxResults(maxResults).getResultList();
		 List<Item> items = new ArrayList<Item>();
		 for (Object[] objects : r) {
			 Item item = (Item) objects[0];
			 item.setDistance((Double) objects[1]);
			 items.add(item);
		 }
		 return items;		 
	 }
	 
	 public static long countItems(Location location, int radius, Set<ItemCategory> itemCategories) {
		 StringBuilder query = new StringBuilder("SELECT COUNT(distinct i.id) FROM ");
		 
		 if (location != null && radius >= 0) {
			 query.append("(SELECT ST_GeomFromText('POINT(" + location.getLongitude() + " " + location.getLatitude() +")', 4326) AS location) AS location, ");
		 }
		 
		 query.append("item i ");
		 
		 Set<Long> categoryIds = new HashSet<Long>();
		 if (itemCategories != null && !itemCategories.isEmpty()) {
			 Stack<ItemCategory> categories = new Stack<ItemCategory>();
			 categories.addAll(itemCategories);
			 while (!categories.isEmpty()) {
				 ItemCategory category = categories.pop();
				 if (!categoryIds.contains(category.getId())) {
					 categoryIds.add(category.getId());
					 categories.addAll(category.getChildItemCategories());
				 }
			 }
			 query.append("INNER JOIN item_item_categories iic ON iic.item = i.id INNER JOIN item_category ic ON ic.id = iic.item_categories WHERE ic.id IN (:categoryids)");
		 } 
		 
		 if (location != null && radius >= 0) {
			 if (categoryIds.isEmpty()) {
				 query.append("WHERE ");
			 } else {
				 query.append("AND ");
			 }
			 query.append("ST_Distance_Sphere(location, ST_GeomFromText('POINT(' || longitude || ' ' || latitude || ')', 4326)) <= :radius ");
		 } 
		 
		 Query q = entityManager().createNativeQuery(query.toString());
		 if (location != null && radius >= 0) {
			 q.setParameter("radius", radius);
		 }
		 if (!categoryIds.isEmpty()) {
			 q.setParameter("categoryids", categoryIds);
		 }
		 
		 return new Long(((Number) q.getSingleResult()).longValue());		 
	 }
	 
	 //ngocnt. Get the ALL items belonging to Attraction category (have and haven't been rated).
	 public static List<Item> findCandidateAttractionItems() {
		 EntityManager em = Item.entityManager();
		 TypedQuery<Item> q = em.createQuery("SELECT o "
				 + "FROM Item AS o "
				 + "WHERE o.remoteSystemInfo.remoteSystemName='lcs-poi' "
//				 + "WHERE (o.remoteSystemInfo.remoteSystemName='lcs-poi' OR o.remoteSystemInfo.remoteSystemName='lcs-gastronomic') "
				 + "AND (o.remoteSystemInfo.remoteSystemName!='') "
				 + "AND (o.averageRating>3 OR o.averageRating IS NULL)"
				 + "ORDER BY o.averageRating", Item.class);
//		 return q.setMaxResults(20).getResultList();
		 return q.getResultList();
	 }
	 
	 
	 public static List<Item> findAllAttractionItems() {
		 EntityManager em = Item.entityManager();
		 TypedQuery<Item> q = em.createQuery("SELECT o "
				 + "FROM Item AS o "
				 + "WHERE o.remoteSystemInfo.remoteSystemName='lcs-poi'", Item.class);
//				 + "WHERE o.remoteSystemInfo.remoteSystemName='lcs-poi' OR o.remoteSystemInfo.remoteSystemName='lcs-gastronomic'", Item.class);
		 return q.getResultList();
	 }

	 public enum Sort {

		 NAME("name"), DISTANCE("distance"), AVERAGE_RATING("averagerating");

		 private final String value;

		 Sort(String value) {
			 this.value = value;
		 }

		 public static Sort fromValue(String value) {
			 for (Sort sort : values()) {
				 if (sort.value.equals(value)) {
					 return sort;
				 }
			 }
			 throw new IllegalArgumentException("Invalid value: " + value);
		 }

		 public String toSort() {
			 return value;
		 }
	 }

	 public enum SuggestionSort {

		 RELEVANCE("relevance"), DISTANCE("distance"), AVERAGE_RATING("averagerating");

		 private final String value;

		 SuggestionSort(String value) {
			 this.value = value;
		 }

		 public static SuggestionSort fromValue(String value) {
			 for (SuggestionSort sort : values()) {
				 if (sort.value.equals(value)) {
					 return sort;
				 }
			 }
			 throw new IllegalArgumentException("Invalid value: " + value);
		 }

		 public String toSuggestionSort() {
			 return value;
		 }
	 }
}