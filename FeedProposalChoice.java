package it.unibz.sipai.domain;
import java.util.Date;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;
import org.springframework.roo.addon.json.RooJson;

@RooJavaBean
@RooToString
@RooJson
@RooJpaActiveRecord(versionField = "", finders = { "findFeedProposalChoicesByFeedProposalAndUserEntity", "findFeedProposalChoicesByFeedProposal", "findFeedProposalChoicesByUserEntity" })
public class FeedProposalChoice {

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date creationDate;

    private Boolean isLike;

    private Boolean isDislike;

    private Boolean isChosen;

    @ManyToOne
    private UserEntity userEntity;

    /**
     */
    @NotNull
    @ManyToOne
    private FeedProposal feedProposal;
}
