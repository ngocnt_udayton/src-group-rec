package it.unibz.sipai.rec.item;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Set;

import com.google.common.base.Functions;
import com.google.common.collect.ImmutableSortedMap;
import com.google.common.collect.Ordering;
import com.joptimizer.functions.LinearMultivariateRealFunction;
import com.joptimizer.functions.SOCPLogarithmicBarrier;
import com.joptimizer.functions.SOCPLogarithmicBarrier.SOCPConstraintParameters;
import com.joptimizer.optimizers.BarrierMethod;
import com.joptimizer.optimizers.LPOptimizationRequest;
import com.joptimizer.optimizers.LPPrimalDualMethod;
import com.joptimizer.optimizers.OptimizationRequest;

import it.unibz.sipai.domain.FeedProposalChoice;
import it.unibz.sipai.domain.Item;
import it.unibz.sipai.domain.RemoteSystemInfo;
import it.unibz.sipai.domain.Review;
import it.unibz.sipai.domain.UserEntity;

public class GroupItemRecommender{

	private static final double PRIORITY_SCORE = 0.0;
	private static final double GROUP_WEIGHT = 1.0;
	private static final double INIT = 0.0000001;
	private static final double BALANCE_SCORE = 0.6;
	//the number of items
	private int numItems;

	private int ratingCount; // Current number of loaded ratings
	private Data ratings[]; // Array of ratings data
	private List<Item> mItemEntities;// List of items
	private Set<UserEntity> mUserEntities;// Set of all users

	private Set<String> mUserEntitiesWithRatings = new HashSet<String>(); //Set of users having at least one rating

	private byte mItemFeatures[][]; // Array of features by item / the matrix of x^(i)
	private double mUserFeatures[][]; // Array of features by user / the matrix of w^(u), compatible with the user constraints
	private double mItemDistanceMatrix[][];


	//for mapping from userId and itemId to index
	private Map<String, Integer> mUserIds = new HashMap<String, Integer>();
	private Map<Long, Integer> mItemIds = new HashMap<Long, Integer>();
	private Map<String, Map<Integer, double[]>> mUserConstraints = new HashMap<String, Map<Integer, double[]>>(); //individual constraints
	private double[] mGroupFeature;
	private List<String> mFeatures = new ArrayList<String>();

	//the number of features, n-dimensional
	private int numFeatures;
	private int numDescriptionFeatures; //feature extracted manually

	float globalAverage;
	private List<Review> mReviews;

	//
	private static int numFavoriteConstraints = 0;
	private static int numLikedConstraints = 0;
	private FeatureExtractorRep featureExtractor;


	public GroupItemRecommender(List<Review> reviews, List<Item> attractionItems, List<UserEntity> userEntities){
		mReviews = reviews;
		mItemEntities= new ArrayList<Item>(attractionItems);
		mUserEntities=new HashSet<UserEntity>(userEntities);
		//load features
		featureExtractor = new FeatureExtractorRep(mItemEntities);
		featureExtractor.init();
		mItemFeatures=featureExtractor.getItemFetures();
		mFeatures=featureExtractor.getListFeatures();
		mItemIds=featureExtractor.getItemIds();
		numDescriptionFeatures=featureExtractor.getDescriptionFeature();
		numFeatures=mFeatures.size();
	}

	public void init(){
		loadArrays();
		calcMetrics();
		calcFeatures();
		computeItemDistanceMatrix();
	}
	public double [] computeGroupUtility(List<UserEntity> groupMembers, List<FeedProposalChoice> groupReviews, int numProposedItem){
		double [] result =  onlineLearnGroupPreference(groupMembers, groupReviews, numProposedItem);
		return result;
	}

	//will be called one time
	public void loadArrays(){
		ratingCount = 0;
		ratings = new Data[mReviews.size()];

		for (Review review : mReviews) {
			Item item = review.getItem();
			long itemId = item.getId();
			UserEntity user = review.getUserEntity();
			String username = user.getUsername();
			int rating = review.getRating();

			mUserEntitiesWithRatings.add(username);


			if (ratings[ratingCount] == null) {
				ratings[ratingCount] = new Data();
			}

			ratings[ratingCount].itemId = itemId;
			ratings[ratingCount].username = username;
			ratings[ratingCount].rating = rating;
			ratings[ratingCount].cache = 0;

			ratingCount++;
		}

		int numUsers = mUserEntities.size();
		numItems = mItemEntities.size();

		mUserFeatures = new double[numFeatures][numUsers]; // matrix of w
		mGroupFeature = new double[numFeatures];
		mItemDistanceMatrix = new double[numItems][numItems];
		for(int f=0;f<mFeatures.size();f++){
			//every value in the x vector, w vector is zero
			for (int i = 0; i < numUsers; i++) mUserFeatures[f][i] = 0;
			mGroupFeature[f]=INIT;
		}
	}

	private void calcMetrics() {
		Integer userIndex;
		Set<Long> itemIdsReviews = new HashSet<Long>();

		// Process each row in the training set
		for (Data rating : ratings) {
			userIndex = mUserIds.get(rating.username);
			if (userIndex == null) {
				userIndex = mUserIds.size();
				// Reserve new id and add lookup
				mUserIds.put(rating.username, userIndex);
			}
		}

		//Mapping for users without ratings
		for(UserEntity user:mUserEntities){
			if(!mUserEntitiesWithRatings.contains(user)){
				String username = user.getUsername();
				userIndex = mUserIds.get(username);
				if (userIndex == null) {
					userIndex = mUserIds.size();
					// Reserve new id and add lookup
					mUserIds.put(username, userIndex);

				}
			}
		}
	}

	// Initialize the item's feature vector, user's vector preference, and set of individual constraints for each user.
	private void calcFeatures() {
		Integer itemIndex, userIndex; // itemIndex=iid; userIndex=uid

		// Loading user preference vector (w)
		// and building constraint for user BEFORE the interaction (rating 1-5)
		for(Iterator<String> u= mUserEntitiesWithRatings.iterator();u.hasNext();){
			String username = u.next();
			userIndex = mUserIds.get(username);
			Set<Long> fiveItemIds = new HashSet<Long>();
			Set<Long> fourItemIds = new HashSet<Long>();
			Set<Long> threeItemIds = new HashSet<Long>();
			Set<Long> twoItemIds = new HashSet<Long>();
			Set<Long> oneItemIds = new HashSet<Long>();
			Set<Long> addedList = new HashSet<Long>();
			Map<Integer, double[]> resConstraints = new HashMap<Integer, double[]>();
			for(int k=0;k<mFeatures.size();k++){
				int featureCount=0;
				for (Data rating : ratings) {
					if(rating.username.equals(username)){
						itemIndex=mItemIds.get(rating.itemId);

						//w:weight vector of user w^u
						if(mItemFeatures[k][itemIndex]==1){
							mUserFeatures[k][userIndex]+=rating.rating;
							featureCount++;
						}

						//get data for building constraints from previous ratings.
						switch(rating.rating){
						case 5: if(!addedList.contains(rating.itemId)){fiveItemIds.add(rating.itemId);addedList.add(rating.itemId);};break;
						case 4: if(!addedList.contains(rating.itemId)){fourItemIds.add(rating.itemId);addedList.add(rating.itemId);};break;
						case 3: if(!addedList.contains(rating.itemId)){threeItemIds.add(rating.itemId);addedList.add(rating.itemId);}break;
						case 2: if(!addedList.contains(rating.itemId)){twoItemIds.add(rating.itemId);addedList.add(rating.itemId);}break;
						case 1: if(!addedList.contains(rating.itemId)){oneItemIds.add(rating.itemId);addedList.add(rating.itemId);}break;
						}
					}
				}//end for data
				if(featureCount!=0)
					mUserFeatures[k][userIndex]=mUserFeatures[k][userIndex]/featureCount;
			}//end feature

			//System.out.println(username);
			List<Set<Long>> listConstraints = new ArrayList<Set<Long>>();
			listConstraints.add(fiveItemIds);
			listConstraints.add(fourItemIds);
			listConstraints.add(threeItemIds);
			listConstraints.add(twoItemIds);
			listConstraints.add(oneItemIds);

			int count = 0;
			double val = 1.0;
			for(int i=0;i<listConstraints.size();i++){
				if(listConstraints.get(i).size()>0){
					for(int j=i+1;j<listConstraints.size();j++){
						if(listConstraints.get(j).size()>0){
							Map<Integer, double[]> setConstraints = null;
							if((i==0 && j==4) || (i==0 && j==3)){
								setConstraints = buildSetConstraints(listConstraints.get(i),listConstraints.get(j), Math.pow(val, 3));
							}else if(i==0 && j==2){
								setConstraints = buildSetConstraints(listConstraints.get(i),listConstraints.get(j), Math.pow(val, 2));
							}else if(i==0 && j==1){
								setConstraints = buildSetConstraints(listConstraints.get(i),listConstraints.get(j), val);
							}else if((i==1 && j==4) || (i==1 && j==3)){
								setConstraints = buildSetConstraints(listConstraints.get(i),listConstraints.get(j), Math.pow(val, 2));
							}else if(i==1 && j==2){
								setConstraints = buildSetConstraints(listConstraints.get(i),listConstraints.get(j), val);
							}else if((i==2 && j==4) || (i==2 && j==3)){
								setConstraints = buildSetConstraints(listConstraints.get(i),listConstraints.get(j), val);
							}else if(i==3 && j==4){
								setConstraints = buildSetConstraints(listConstraints.get(i),listConstraints.get(j), (val));
							}
							setConstraints = buildSetConstraints(listConstraints.get(i),listConstraints.get(j), (val));
							for(double[] constraint:setConstraints.values()){
								resConstraints.put(count, constraint);
								count++;
							}
							break;
						}
					}
				}
			}
			if(resConstraints.size()>0){
				mUserConstraints.put(username, resConstraints);
			}
		}//end for user
	}

	public double[] onlineLearnGroupPreference(List<UserEntity> groupMembers, List<FeedProposalChoice> groupReviews, int numProposedItem){
		Map<String, double[]> mUserFeaturesInGroup = new HashMap<String, double[]>();
		Map<String, double[]> mUserFeaturesOutGroup = new HashMap<String, double[]>();
		Map<String, Map<Integer, double[]>> groupContraints = implyGroupConstraints(groupMembers, groupReviews);
		double [] groupFeatureInGroup = new double[mFeatures.size()];
		double [] groupFeatureOutGroup = new double[mFeatures.size()];
		double [] sumEachFeature = new double[numFeatures];
		double sumOut=0;
		double sumIn=0;
		double [] result = new double[mFeatures.size()];
		Map<String, Double> memberImpactScore = new HashMap<String, Double>();

		for(int k=0;k<mFeatures.size();k++){
			groupFeatureInGroup[k]=0;
			groupFeatureOutGroup[k]=0;
			sumEachFeature[k]=0;
		}

		//1a. Initialization w^G
		for(UserEntity user: groupMembers){
			Integer userIndex = mUserIds.get(user.getUsername());
			for(int k=0;k<mFeatures.size();k++){
				sumEachFeature[k]+=mUserFeatures[k][userIndex];
				sumOut+=mUserFeatures[k][userIndex];
			}
		}
		if(sumOut!=0){
			for(int k=0;k<mFeatures.size();k++){
				groupFeatureOutGroup[k]=sumEachFeature[k]/sumOut;
			}
		}

		//1b.Exception, in case group members haven't had any ratings before, ...
		//balance part
		for(int k=0;k<mFeatures.size();k++){
			sumEachFeature[k]=0;
		}
		for(UserEntity user: groupMembers){
			String username = user.getUsername();
			Map<Integer, double[]> userConstraints = groupContraints.get(username);
			if(userConstraints!=null){
				for(double[] constraint:userConstraints.values()){
					int tmp=0;
					for(int k=0;k<mFeatures.size();k++){
						if(constraint[k]<0){
							tmp++;
						}
					}
					for(int k=0;k<mFeatures.size();k++){
						if(constraint[k]<0){
							double val= 1.0/tmp;
							sumEachFeature[k]+=val;
							sumIn+=val;
						}
					}
				}
			}else{ //if userConstraints == null
				List<Item> listItems = new ArrayList<Item>();
				boolean isFavorite = false;
				boolean isLiked = false;
				boolean isNeutral = false;
				boolean isDisliked = false;

				for(FeedProposalChoice fpc: groupReviews){
					if(fpc.getUserEntity().getUsername().equals(username)){
						if(fpc.getIsChosen()){
							isFavorite=true;
						}else if(fpc.getIsLike()){
							isLiked = true;
						}else if(fpc.getIsDislike()){
							isDisliked = true;
						}else{//neutral
							isNeutral = true;
						}
						listItems.add(fpc.getFeedProposal().getItem());
					}
				}//end for
				if(isFavorite || isLiked){
					for(Item item: listItems){
						Long itemId = item.getId();
						int itemIndex = mItemIds.get(itemId);
						for(int k=0;k<mFeatures.size();k++){
							if(mItemFeatures[k][itemIndex]==1){
								sumEachFeature[k]+=1;
								sumIn+=1;
							}
						}
					}
				}else if(isDisliked){
					double[] tmpSumFeature = generateVector(0.0, numFeatures);
					for(Item item: listItems){
						Long itemId = item.getId();
						int itemIndex = mItemIds.get(itemId);
						for(int k=0;k<mFeatures.size();k++){
							if(mItemFeatures[k][itemIndex]==1){
								tmpSumFeature[k]+=1;
							}
						}
					}
					for(int k=0;k<mFeatures.size();k++){
						if(tmpSumFeature[k]==0){
							sumEachFeature[k]=1;
							sumIn+=1;
						}
					}
				}else if(isNeutral){
					for(int k=0;k<mFeatures.size();k++){
						sumEachFeature[k]=1;
						sumIn+=1;
					}
				}
			}
		}
		if(sumIn!=0){
			for(int k=0;k<mFeatures.size();k++){
				groupFeatureInGroup[k]=sumEachFeature[k]/sumIn;
			}
		}
		//Balance OUT & WITHIN group
		for(int k=0;k<mFeatures.size();k++){
			if(sumIn!=0 && sumOut!=0)
				result[k]=groupFeatureInGroup[k]*BALANCE_SCORE+groupFeatureOutGroup[k]*(1-BALANCE_SCORE);
			else if(sumIn!=0 && sumOut==0)
				result[k]=groupFeatureInGroup[k];
			else if(sumIn==0 && sumOut!=0)
				result[k]=groupFeatureOutGroup[k];
		}

		// Solve optimization problems: constraint BEFORE & DURING interaction
		for(UserEntity user: groupMembers){
			String username = user.getUsername();
			double [] inGroup = solveOptimizationProblemQCQP(username, result, groupContraints.get(username));
//			double [] inGroup = solveOptimizationProblemLP(username, result, groupContraints.get(username));
			mUserFeaturesInGroup.put(username, inGroup);

			double coef = GROUP_WEIGHT;
			if(coef!=1){
				double [] outGroup = solveOptimizationProblemQCQP(username, result, mUserConstraints.get(username));
//				double [] outGroup = solveOptimizationProblemLP(username, result, mUserConstraints.get(username));
				mUserFeaturesOutGroup.put(username, outGroup);
			}else{
				double [] outGroup = new double [mFeatures.size()];
				for(int k=0;k<outGroup.length;k++)
					outGroup[k]=0;
				mUserFeaturesOutGroup.put(username, outGroup);
			}
		}

		//3. Compute new version of w^G
		result = aggregateGroupPreference(groupMembers, mUserFeaturesInGroup, mUserFeaturesOutGroup, memberImpactScore);
		return result;
	}

	// Building constraint for user DURING the interaction (favorite, like, neutral, dislike)
	private Map<String, Map<Integer, double[]>> implyGroupConstraints(List<UserEntity> groupMembers, List<FeedProposalChoice> groupReviews){
		Map<String, Map<Integer, double[]>> groupContraints = new HashMap<String, Map<Integer, double[]>>();
		for(UserEntity mem:groupMembers){
			String username = mem.getUsername();
			Set<Long> favoriteItemIds = new HashSet<Long>();
			Set<Long> likedItemIds = new HashSet<Long>();
			Set<Long> neutralItemIds = new HashSet<Long>();
			Set<Long> dislikedItemIds = new HashSet<Long>();
			Map<Integer, double[]> resConstraints = new HashMap<Integer, double[]>();

			for(FeedProposalChoice groupReview: groupReviews){
				Long itemId = groupReview.getFeedProposal().getItem().getId();
				if(groupReview.getUserEntity().getUsername().equals(username)){
					if(groupReview.getIsChosen()==true){
						favoriteItemIds.add(itemId);
					}else if (groupReview.getIsLike()==true){
						likedItemIds.add(itemId);
					}else if(groupReview.getIsDislike()==true){
						dislikedItemIds.add(itemId);
					}else{
						neutralItemIds.add(itemId);
					}
				}
			}//end for FeeProposalChoice

			//group constraint
			List<Set<Long>> listConstraints = new ArrayList<Set<Long>>();
			listConstraints.add(favoriteItemIds);
			listConstraints.add(likedItemIds);
			listConstraints.add(neutralItemIds);
			listConstraints.add(dislikedItemIds);

			int count=0;
			double val = 1;
			for(int i=0;i<listConstraints.size();i++){
				if(listConstraints.get(i).size()>0){
					for(int j=i+1;j<listConstraints.size();j++){
						if(listConstraints.get(j).size()>0){
							Map<Integer, double[]> setConstraints=null;
							if(i==0 && j==3){ //love - dislike
								setConstraints = buildSetConstraints(listConstraints.get(i),listConstraints.get(j), Math.pow(val, 3));
							}else if(i==0 && j==2){ //love-neutral
								setConstraints = buildSetConstraints(listConstraints.get(i),listConstraints.get(j), Math.pow(val, 2));
							}else if(i==0 && j==1){ //love-like
								setConstraints = buildSetConstraints(listConstraints.get(i),listConstraints.get(j), val);
							}else if(i==1 && j==3){//like-dislike
								setConstraints = buildSetConstraints(listConstraints.get(i),listConstraints.get(j), Math.pow(val, 2));
							}else if(i==1 && j==2){//like-neutral
								setConstraints = buildSetConstraints(listConstraints.get(i),listConstraints.get(j), val);
							}else if(i==2 && j==3){//neutral - dislike
								setConstraints = buildSetConstraints(listConstraints.get(i),listConstraints.get(j), val);
							}

							if(i==0){
								numFavoriteConstraints = setConstraints.size();
							}
							if(i==1){
								numLikedConstraints = setConstraints.size();
							}
							for(double[] constraint:setConstraints.values()){
								resConstraints.put(count, constraint);
								count++;
							}
							break;
						}
					}
				}
			}
			if(resConstraints.size()>0){
				groupContraints.put(username, resConstraints);
			}
		}//end for user Member
		return groupContraints;
	}

	//Solve optimization problem
	private double[] solveOptimizationProblemLP(String username, double[] groupFeature, Map<Integer, double[]> constraints){
		//Objective function
		double[] c = new double[numFeatures];

		//Bounds on variables
		double[] lb = new double[numFeatures];
		double[] ub = new double[numFeatures];


		//Inequalities constraints
		double[][] G = null;
		double[] h = null;
		double[][] A = new double[1][numFeatures];
		double[] b = new double[] {1.0};
		if(constraints!=null){
			G = new double[constraints.size()][numFeatures];
			//Assign value
			int constraintIndex = 0;
			for(double[] constraint:constraints.values()){
				for(int k=0;k<mFeatures.size();k++){
					G[constraintIndex][k]=constraint[k];
				}
				constraintIndex++;
			}
		}else{ //no constraints
			G = new double[1][numFeatures];
			for(int k=0;k<mFeatures.size();k++){
				G[0][k]=0.0;
			}
		}

		for(int k=0;k<mFeatures.size();k++){
			c[k]=-groupFeature[k];
			lb[k]=0.0;
			ub[k]=1.0;
			A[0][k]=1.0;
		}
		h = new double [G.length];
		for(int i=0;i<h.length;i++){
//			if(i<numFavoriteConstraints)
//				h[i]=-0.2;
//			else
//				h[i]=0.0;
			h[i]=0.0;
		}
		//optimization problem
		LPOptimizationRequest or = new LPOptimizationRequest();
		or.setC(c);
		or.setG(G);
		or.setH(h);
		or.setLb(lb);
		or.setUb(ub);
		or.setA(A);
		or.setB(b);
		or.setDumpProblem(true);

		//optimization
		LPPrimalDualMethod opt = new LPPrimalDualMethod();

		opt.setLPOptimizationRequest(or);
		double[] sol=null;
		try {
			int returnCode = opt.optimize();
			sol= opt.getOptimizationResponse().getSolution();

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		if (sol==null){
			sol = new double[mFeatures.size()];
			for(int k=0;k<mFeatures.size();k++){
				sol[k] = 0;
			}
			System.out.println("No solution for "+username);
		}
		//		System.out.println("Vector preference (after optimization): "+username);
		//		printVector(sol);
		return sol;
	}

	//Linear combination & aggregate
	private double[] aggregateGroupPreference(List<UserEntity> groupMembers, Map<String, double[]> memberFeaturesInGroup, Map<String, double[]> memberFeaturesOutGroup, Map<String, Double> memberImpactScore){
		double [] groupFeature = new double[numFeatures];
		double [][] temp = new double[groupMembers.size()][numFeatures];
		Map<String, double[]> memberFeaturesFinal = new HashMap<String, double[]>();

		//Combination
		int i = 0;
		for(UserEntity member:groupMembers){
			String username = member.getUsername();
			double[] in = memberFeaturesInGroup.get(username);
			double[] out = memberFeaturesOutGroup.get(username);
			for(int k=0;k<mFeatures.size();k++){
				temp[i][k]=GROUP_WEIGHT*in[k] + (1-GROUP_WEIGHT)*out[k];
			}
			memberFeaturesFinal.put(username, temp[i]);
			i++;
		}

		//Average aggregation
		for(UserEntity member:groupMembers){
			String username = member.getUsername();
			//			System.out.println("Vector preference (after optimization): "+username);
			//			printVector(memberFeaturesFinal.get(username));
			for(int k=0;k<mFeatures.size();k++){
				groupFeature[k]+=memberFeaturesFinal.get(username)[k];
			}
		}
		for(int k=0;k<mFeatures.size();k++){
			groupFeature[k]= groupFeature[k]/groupMembers.size();
		}
		return groupFeature;
	}

	//build constraint for each user
	private Map<Integer, double[]> buildSetConstraints(Set<Long> likedItemIds, Set<Long> dislikedItemIds, double coeff){
		byte[] likedFeatures = new byte[numFeatures];
		byte[] dislikedFeatures = new byte[numFeatures];
		Map<Integer, double[]> setConstraints = new HashMap<Integer, double[]>();
		//Get pair
		int count = 0;
		for(Long likedItemId:likedItemIds){
			int likedItemIndex = mItemIds.get(likedItemId);
			for(Long dislikedItemId:dislikedItemIds){
				int dislikeditemIndex = mItemIds.get(dislikedItemId);
				for(int k=0;k<mFeatures.size();k++){
					likedFeatures[k]=mItemFeatures[k][likedItemIndex];
					dislikedFeatures[k]=mItemFeatures[k][dislikeditemIndex];
				}//end for k
				double[] constraint = buildConstraint(likedFeatures, dislikedFeatures, coeff);
				setConstraints.put(count,constraint);
				count++;
			}
		}
		return setConstraints;
	}

	private double[] buildConstraint(byte[] preferedFeatures, byte[] otherFeatures, double coeff){
		double[] constraint = new double[numFeatures];
		for(int k=0;k<mFeatures.size();k++){
			if(preferedFeatures[k]==1 && otherFeatures[k]==0)
				constraint[k]=-coeff;//-1;
			else if(preferedFeatures[k]==1 && otherFeatures[k]==1)
				constraint[k]=0;
			else if(preferedFeatures[k]==0 && otherFeatures[k]==0)
				constraint[k]=0;
			else if(preferedFeatures[k]==0 && otherFeatures[k]==1)
				constraint[k]=1;
		}
		return constraint;
	}

	private void printSetConstraints(Map<Integer, byte[]> setConstraints){
		//Output, print test
		for(byte[] constraint:setConstraints.values()){
			printVector(constraint);
		}

	}


	private void printVector(byte[] x){
		for(int k=0;k<mFeatures.size();k++){
			System.out.print(x[k]+"\t");
		}
		System.out.println();
	}

	private void printVector(double[] x, boolean isNegative){
		for(int k=0;k<mFeatures.size();k++){
			if(isNegative){
				if(x[k]<0)
					System.out.print(x[k]+"\t");
			}else{
				System.out.print(x[k]+"\t");
			}
		}
		System.out.println();
	}

	private void printVector(List<String> list){
		for (int k=0;k<list.size();k++) {
			System.out.print(list.get(k)+"\t");
		}
		System.out.println();
	}

	private boolean searchString(String [] doc, String keyword){
		for(int i=0;i<doc.length;i++){
			if(doc[i].indexOf(keyword)>=0 && doc[i].substring(0, 1).equals(keyword.substring(0, 1))){
				return true;
			}
		}
		return false;
	}


	// Find all similar items to what have proposed.
	private void computeItemDistanceMatrix(){
		CosineSimilarity cosineSim = new CosineSimilarity();
		byte[] i1 = new byte[numFeatures];
		byte[] i2 = new byte[numFeatures];
		for(int i=0; i<numItems;i++){
			for(int j=i+1;j<numItems;j++){
				for(int k=0;k<numFeatures;k++){
					i1[k]=mItemFeatures[k][i];
					i2[k]=mItemFeatures[k][j];
				}
				mItemDistanceMatrix[i][j] = cosineSim.calculateCosineSimilarity(i1, i2);
				mItemDistanceMatrix[j][i] = mItemDistanceMatrix[i][j];
				mItemDistanceMatrix[i][i] = 1;
			}
		}
	}


	//Solve optimization problem
		private double[] solveOptimizationProblemQCQP(String username, double[] groupFeature, Map<Integer, double[]> constraints){
			double[] sol=null;
			// Objective function (plane)
			double[] c = new double[numFeatures];
			for(int k=0;k<mFeatures.size();k++){
				c[k]=-groupFeature[k];
			}
			//Inequalities constraints
			double[][] G = null;
			double[] h = null;
			if(constraints!=null){
				G = new double[constraints.size()][numFeatures];
				//Assign value
				int constraintIndex = 0;
				for(double[] constraint:constraints.values()){
					for(int k=0;k<mFeatures.size();k++){
						G[constraintIndex][k]=constraint[k];
					}
					constraintIndex++;
				}
			}else{ //no constraints
				G = new double[1][numFeatures];
				for(int k=0;k<mFeatures.size();k++){
					G[0][k]=0.0;
				}
			}
//			writeFile(G,groupFeature,"/Users/user/temp/testQuadratic_"+username+".txt");
			LinearMultivariateRealFunction objectiveFunction = new LinearMultivariateRealFunction(c, 0);
			List<SOCPConstraintParameters> socpConstraintParametersList = new ArrayList<SOCPLogarithmicBarrier.SOCPConstraintParameters>();
			SOCPLogarithmicBarrier barrierFunction = new SOCPLogarithmicBarrier(socpConstraintParametersList, numFeatures);

	//      second order cone constraint in the form ||A1.x+b1||<=c1.x+d1,
			double[][] A1 = generateIdentityMatrix(numFeatures);
			double[] b1 = generateVector(0.0, numFeatures);
			double[] c1 = generateVector(0.0, numFeatures);
			double d1 = 1.0;
			SOCPConstraintParameters constraintParams1 = barrierFunction.new SOCPConstraintParameters(A1, b1, c1, d1);
			socpConstraintParametersList.add(socpConstraintParametersList.size(), constraintParams1);


	//      second order cone constraint in the form ||A2.x+b2||<=c2.x+d2,
			if(constraints!=null){
				for(int i=0;i<G.length;i++){
					double[][] A2 = new double[1][numFeatures];
					for(int k=0;k<numFeatures;k++){
						A2[0][k]=0;
					}
					double[] b2 = new double[] {0};
					double[] c2 = new double[numFeatures];
					for(int j=0;j<numFeatures;j++){
						c2[j]=-G[i][j];
					}
					double d2 = 0.0;
					SOCPConstraintParameters constraintParams2 = barrierFunction.new SOCPConstraintParameters(A2, b2, c2, d2);
					socpConstraintParametersList.add(socpConstraintParametersList.size(), constraintParams2);
				}
			}


			//optimization problem
			OptimizationRequest or = new OptimizationRequest();
			or.setF0(objectiveFunction);
	//		or.setInitialPoint(groupFeature);
			or.setCheckProgressConditions(true);

			//optimization
			BarrierMethod opt = new BarrierMethod(barrierFunction);
			opt.setOptimizationRequest(or);
			try {
				int returnCode = opt.optimize();
				sol = opt.getOptimizationResponse().getSolution();
			} catch (Exception e) {
//				e.printStackTrace();
				sol = new double[mFeatures.size()];
				for(int k=0;k<mFeatures.size();k++){
					sol[k] = groupFeature[k];
				}
				System.out.println("No solution for "+username);
			}
			double sum=0;
			for(int k=0;k<numFeatures;k++){
				if(sol[k]<0)
					sol[k]=0;
				else
					sum+=sol[k];
			}
			if(sum!=0){
				for(int k=0;k<numFeatures;k++){
					sol[k]=sol[k]/sum;
				}
			}
//			System.out.println("After normalization "+username);
//			printVector(sol, false);
			return sol;
		}

	//create IdentityMatrix
	private double[][] generateIdentityMatrix(int dim){
		double[][] res = new double[dim][dim];
		for(int i=0;i<dim;i++){
			for(int j=0;j<dim;j++){
				res[i][j]=i==j?1.0:0.0;
			}
		}
		return res;
	}
	private double[] generateVector(double value, int dim){
		double[] res = new double[dim];
		for(int i=0;i<dim;i++){
			res[i]=value;
		}
		return res;
	}
}
