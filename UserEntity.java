package it.unibz.sipai.domain;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Embedded;
import javax.persistence.EntityManager;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.Query;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.json.RooJson;
import org.springframework.roo.addon.tostring.RooToString;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import flexjson.JSON;
import flexjson.JSONDeserializer;
import flexjson.JSONSerializer;
import flexjson.transformer.DateTransformer;

@RooJavaBean
@RooToString
@RooJpaActiveRecord(versionField = "")
@RooJson
public class UserEntity implements UserDetails {
	private static final long serialVersionUID = 1L;

    @Id
    private String username;
    
    private String password;

    private Character gender;

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date birthDate;

    @Embedded
    private Location location;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userEntity")
    @OrderBy("id")
    private Set<ContextSetting> contextSettings = new HashSet<ContextSetting>();

    @OneToMany(cascade = CascadeType.REMOVE, mappedBy = "userEntity")
    @OrderBy("id")
    private Set<BookmarkedItem> bookmarkedItems = new HashSet<BookmarkedItem>();

    @OneToMany(cascade = CascadeType.REMOVE, mappedBy = "userEntity")
    private Set<Review> reviews = new HashSet<Review>();
    
    @OneToOne(cascade = CascadeType.REMOVE, mappedBy = "userEntity")
    private UserPersonality userPersonality;
    
    @OneToMany(cascade = CascadeType.REMOVE, mappedBy = "userEntity")
    private Set<SkippedItem> skippedItems = new HashSet<SkippedItem>();
             
    private String experimentalGroup;
    
    private transient List<Item> lastReviewedItems;
        
    @JSON(include = false)
    @Override
    public Collection<GrantedAuthority> getAuthorities() {
        return null;
    }

    @JSON(include = false)
    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @JSON(include = false)
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @JSON(include = false)
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @JSON(include = false)
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @JSON(include = false)
    @Override
    public boolean isEnabled() {
        return true;
    }
    
    @JSON(include = false)
    public String getExperimentalGroup() {
		return experimentalGroup;
	}
    
    public boolean getActiveLearningTreatmentGroup() {
    	if (experimentalGroup != null && experimentalGroup.length() == 2) {
    		return experimentalGroup.charAt(0) == '1';
    	} 
    	return true;
    }
    
    public boolean getWeatherTreatmentGroup() {
    	if (experimentalGroup != null && experimentalGroup.length() == 2) {
    		return experimentalGroup.charAt(1) == '1';
    	} 
    	return true;
    }
    
    @JSON(include = false)
    public Set<SkippedItem> getSkippedItems() {
		return skippedItems;
	}
    
    public List<Item> getLastReviewedItems() {
		return lastReviewedItems;
	}
    
    public void setLastReviewedItems(List<Item> lastReviewedItems) {
		this.lastReviewedItems = lastReviewedItems;
	}
        
    public static UserEntity fromJsonToUserEntity(String json) {
        return new JSONDeserializer<UserEntity>().use(null, UserEntity.class).use(Date.class, new DateTransformer("dd/MM/yyyy")).deserialize(json);
    }
    
    public String toJson() {
        return new JSONSerializer().transform(new DateTransformer("dd/MM/yyyy"), Date.class).exclude("*.class", "contextSettings.selectedContextValue.contextFactor", "contextSettings.contextFactor.contextValues.contextFactor").include("lastReviewedItems", "contextSettings", "contextSettings.contextFactor.contextValues", "userPersonality.personalityTraitScores").serialize(this);
    }

    public static String toJsonArray(Collection<UserEntity> collection) {
        return new JSONSerializer().transform(new DateTransformer("dd/MM/yyyy"), Date.class).exclude("*.class", "contextSettings.selectedContextValue.contextFactor", "contextSettings.contextFactor.contextValues.contextFactor").include("lastReviewedItems", "contextSettings", "contextSettings.contextFactor.contextValues", "userPersonality.personalityTraitScores").serialize(collection);
    }
    
//    public static String toJsonArrayShort(Collection<UserEntity> collection) {
//        return new JSONSerializer().transform(new DateTransformer("dd/MM/yyyy"), Date.class).exclude("*.class", "contextSettings.selectedContextValue.contextFactor", "contextSettings.contextFactor.contextValues.contextFactor","lastReviewedItems", "contextSettings", "contextSettings.contextFactor.contextValues", "userPersonality.personalityTraitScores").include().serialize(collection);
//    }
    
    //ngocnt.
    /**
     */
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userEntity")
    @OrderBy("id")
    private Set<FriendSetting> friendSettings = new HashSet<FriendSetting>();

    /**
     */
    @OneToMany(cascade = CascadeType.REMOVE, mappedBy = "userEntity")
    private Set<FeedProposal> feedProposals = new HashSet<FeedProposal>();
    
    //findAllUsers
    @SuppressWarnings("unchecked")
	 public static List<String> findAllUsers(String username) {
		 StringBuilder query = new StringBuilder();
		 //*select UPPER(LEFT(username,1))||LOWER(SUBSTRING(username,2,LENGTH(username)-1)) username
//		 query.append("SELECT username || '&' || gender FROM user_entity WHERE username "+
//				 		"NOT IN (SELECT friend_entity FROM friend_setting WHERE user_entity='"+username+"') "+
//				 		" AND username <> '"+username+"' AND gender is not null ORDER BY username");
//		 query.append("SELECT username || '&' || coalesce(gender,'m') || '&' || coalesce(birth_date,'1900/01/01') FROM user_entity WHERE username "+
//			 		"NOT IN (SELECT friend_entity FROM friend_setting WHERE user_entity='"+username+"') "+
//			 		" AND username <> '"+username+"' ORDER BY username");
		 query.append("SELECT username || '&' || coalesce(gender,'m') || '&' || coalesce(birth_date,'1900/01/01') FROM user_entity WHERE username "+
			 		"NOT IN (SELECT friend_entity FROM friend_setting WHERE user_entity='"+username+"') "+
//			 		"AND username NOT IN (SELECT user_entity FROM friend_setting WHERE friend_entity='"+username+"' AND is_confirmed=TRUE) "+
					"AND username NOT IN (SELECT user_entity FROM friend_setting WHERE friend_entity='"+username+"') "+
			 		"AND username <> '"+username+"' ORDER BY username");
		 EntityManager em = entityManager();
		 Query q = em.createNativeQuery(query.toString());
		 List<Object> resultList = (List<Object>) q.getResultList();
		 List<String> userEntities = new ArrayList<String>();
		 for (Object result : resultList) {
			 userEntities.add(String.valueOf(result.toString()));
		 }
		 return userEntities;
	 }
    
    //findAllActiveFriends
    @SuppressWarnings("unchecked")
    public static List<String> findAllActiveFriends(String username) {
		 StringBuilder query = new StringBuilder();
		 query.append("SELECT friend_entity FROM friend_setting WHERE user_entity='"+username+"' "+
				 	  " AND is_enable=true "+
				 	  " AND friend_entity NOT IN (SELECT user_entity "+
				 	  							  "FROM feed_privilege "+ 
				 	  							  "WHERE is_joined=TRUE "+ 
				 	  							  "AND feed_proposal NOT IN (SELECT feed_proposal FROM feed_privilege WHERE user_entity='"+username+"'))");
		 EntityManager em = entityManager();
		 Query q = em.createNativeQuery(query.toString());
		 List<Object> resultList = (List<Object>) q.getResultList();
		 List<String> friendEntities = new ArrayList<String>();
		 for (Object result : resultList) {
			 friendEntities.add(String.valueOf(result.toString()));
		 }
		 return friendEntities;
	 }
    //findAllFriends
    @SuppressWarnings("unchecked")
    public static List<String> findAllFriends(String username) {
		 StringBuilder query = new StringBuilder();
		 query.append("SELECT friend_entity FROM friend_setting WHERE user_entity='"+username+"'");
		 EntityManager em = entityManager();
		 Query q = em.createNativeQuery(query.toString());
		 List<Object> resultList = (List<Object>) q.getResultList();
		 List<String> friendEntities = new ArrayList<String>();
		 for (Object result : resultList) {
			 friendEntities.add(String.valueOf(result.toString()));
		 }
		 return friendEntities;
	 }
    //findFriendsInDiscussion
    @SuppressWarnings("unchecked")
    public static List<String> findFriendsInDiscussion(String username) {
		 StringBuilder query = new StringBuilder();
		 query.append("SELECT DISTINCT user_entity "+
				 	  "FROM feed_privilege "+
				 	  "WHERE feed_proposal IN (SELECT feed_proposal FROM feed_privilege WHERE user_entity='"+username+"' AND is_joined=TRUE) "+
				 	  		" AND user_entity!='"+username+"' AND is_joined=TRUE "+
				 	  		"AND user_entity IN (SELECT friend_entity FROM friend_setting WHERE user_entity='"+username+"' AND is_confirmed = TRUE)");
		 EntityManager em = entityManager();
		 Query q = em.createNativeQuery(query.toString());
		 List<Object> resultList = (List<Object>) q.getResultList();
		 List<String> friendEntities = new ArrayList<String>();
		 for (Object result : resultList) {
			 friendEntities.add(String.valueOf(result.toString()));
		 }
		 return friendEntities;
	 }
}