package it.unibz.sipai.domain;

import it.unibz.sipai.util.GroupTransformer;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OrderBy;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.json.RooJson;
import org.springframework.roo.addon.tostring.RooToString;
import org.springframework.transaction.annotation.Transactional;

import flexjson.JSONSerializer;
import flexjson.transformer.DateTransformer;

@RooJavaBean
@RooToString
@RooJson
@RooJpaActiveRecord(versionField = "", finders = { "findReviewsByItemAndUserEntity", "findReviewsByItem", "findReviewsByUserEntity" })
public class Review implements Comparable<it.unibz.sipai.domain.Review> {

    private String comment;

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private Date creationDate;

    @NotNull
    private Integer rating;

    @ManyToOne
    private UserEntity userEntity;

    @ManyToMany
    @OrderBy("id")
    private Set<ContextValue> contextValues = new HashSet<ContextValue>();

    @ManyToOne
    @NotNull
    private Item item;

    @PersistenceContext
    transient EntityManager entityManager;

    public String toJson() {
        return new JSONSerializer().transform(new DateTransformer("dd/MM/yyyy"), Date.class).exclude("*.class", "userEntity", "item").include("contextValues").serialize(this);
    }

    public static String toJsonArray(Collection<it.unibz.sipai.domain.Review> collection) {
        return new JSONSerializer().transform(new GroupTransformer(), Group.class).transform(new DateTransformer("dd/MM/yyyy"), Date.class).exclude("*.class", "userEntity", "item").include("contextValues").serialize(collection);
    }
    
    public static List<it.unibz.sipai.domain.Review> findCommentReviewEntriesByItem(Item item, Long startAt, int firstResult, int maxResults) {
        if (item == null) throw new IllegalArgumentException("The item argument is required");
        StringBuilder query = new StringBuilder("SELECT o FROM Review AS o WHERE o.item = :item AND o.comment IS NOT NULL");
        if (startAt != null) {
            query.append(" AND o.id <= :startAt");
        }
        query.append(" ORDER BY o.id DESC");
        EntityManager em = Review.entityManager();
        TypedQuery<Review> q = em.createQuery(query.toString(), Review.class);
        q.setParameter("item", item);
        if (startAt != null) {
            q.setParameter("startAt", startAt);
        }
        return q.setFirstResult(firstResult).setMaxResults(maxResults).getResultList();
    }
    
    public static List<Review> findReviewsByUserEntityAndSinceCreationDate(UserEntity userEntity, Date creationDate) {
    	if (userEntity == null) throw new IllegalArgumentException("The userEntity argument is required");
    	if (creationDate == null) throw new IllegalArgumentException("The creationDate argument is required");
    	EntityManager em = Review.entityManager();
        TypedQuery<Review> q = em.createQuery("SELECT o FROM Review AS o WHERE o.userEntity = :userEntity AND o.creationDate > :creationDate", Review.class);
        q.setParameter("userEntity", userEntity);   
        q.setParameter("creationDate", creationDate);
        return q.getResultList();
    }
    
    public static List<Review> findReviewsForTripAdvisorItems() {
    	EntityManager em = Review.entityManager();
        TypedQuery<Review> q = em.createQuery("SELECT o FROM Review AS o WHERE o.item IN (SELECT t.item FROM TripAdvisorItem AS t)", Review.class);
        return q.getResultList();
    }
    
    public static List<Review> findBinaryReviewsForTripAdvisorItems() {
    	List<Object[]> rows = entityManager().createNativeQuery("SELECT ui.user_entity, ui.item, (SELECT CASE WHEN r.rating IS NULL THEN 1 ELSE 2 END) AS rating FROM (SELECT * FROM (SELECT DISTINCT(user_entity) from review r JOIN trip_advisor_item t ON r.item = t.item) AS user_entity, (SELECT DISTINCT(r.item) from review r JOIN trip_advisor_item t ON r.item = t.item) AS item) ui LEFT JOIN review r ON ui.user_entity = r.user_entity AND ui.item = r.item").getResultList();
    	List<UserEntity> users = entityManager().createQuery("SELECT distinct(o.userEntity) FROM Review AS o WHERE o.item IN (SELECT t.item FROM TripAdvisorItem AS t)", UserEntity.class).getResultList();
    	Map<String, UserEntity> usernames = new HashMap<String, UserEntity>();
    	for (UserEntity userEntity : users) {
			usernames.put(userEntity.getUsername(), userEntity);
		}
    	List<Item> items = entityManager().createQuery("SELECT DISTINCT(o.item) from Review AS o WHERE o.item IN (SELECT t.item FROM TripAdvisorItem AS t)", Item.class).getResultList();
    	Map<Long, Item> itemIds = new HashMap<Long, Item>();
    	for (Item item : items) {
			itemIds.put(item.getId(), item);
		}
    	List<Review> reviews = new ArrayList<Review>();
    	for (Object[] row : rows) {
    		Review review = new Review();
    		review.setUserEntity(usernames.get(String.valueOf(row[0])));
    		review.setItem(itemIds.get(Long.parseLong(String.valueOf(row[1]))));
    		review.setRating(Integer.parseInt(String.valueOf(row[2])));
			reviews.add(review);
		}
    	return reviews;
    }
    
    public static List<Review> findReviewsUntilCreationDate(Date creationDate) {
    	if (creationDate == null) throw new IllegalArgumentException("The creationDate argument is required");
    	EntityManager em = Review.entityManager();
        TypedQuery<Review> q = em.createQuery("SELECT o FROM Review AS o WHERE o.creationDate < :creationDate", Review.class);
        q.setParameter("creationDate", creationDate);
        return q.getResultList();
    }

    public static long countCommentReviewsByItem(Item item, Long startAt) {
        if (item == null) throw new IllegalArgumentException("The item argument is required");
        StringBuilder query = new StringBuilder("SELECT COUNT(*) FROM Review AS o WHERE o.item = :item AND o.comment IS NOT NULL");
        if (startAt != null) {
            query.append(" AND o.id <= :startAt");
        }
        EntityManager em = Review.entityManager();
        TypedQuery<Long> q = em.createQuery(query.toString(), Long.class);
        q.setParameter("item", item);
        if (startAt != null) {
            q.setParameter("startAt", startAt);
        }
        return q.getSingleResult();
    }
    
    public static long countReviewsByUserEntity(UserEntity userEntity) {
        if (userEntity == null) throw new IllegalArgumentException("The userEntity argument is required");
        EntityManager em = Review.entityManager();
        TypedQuery<Long> q = em.createQuery("SELECT COUNT(*) FROM Review AS o WHERE o.userEntity = :userEntity", Long.class);
        q.setParameter("userEntity", userEntity);
        return q.getSingleResult();
    }

    public static Double findAverageRatingByItem(Item item) {
        if (item == null) throw new IllegalArgumentException("The item argument is required");
        EntityManager em = Music2ItemSimilarity.entityManager();
        TypedQuery<Double> q = em.createQuery("SELECT AVG(o.rating) FROM Review AS o WHERE o.item = :item", Double.class);
        q.setParameter("item", item);
        List<Double> resultList = q.getResultList();
        if (resultList.isEmpty()) {
            return null;
        } else {
            return resultList.get(0);
        }
    }
    
    public static List<Review> findLastReviewsByUserEntity(UserEntity userEntity, int maxResults) {
        if (userEntity == null) throw new IllegalArgumentException("The userEntity argument is required");
        EntityManager em = Review.entityManager();
        TypedQuery<Review> q = em.createQuery("SELECT o FROM Review AS o WHERE o.userEntity = :userEntity ORDER BY o.creationDate DESC", Review.class);
        q.setParameter("userEntity", userEntity);
        return q.setFirstResult(0).setMaxResults(maxResults).getResultList();
    }

    @Transactional
    public void persist() {
        if (entityManager == null) entityManager = entityManager();
        entityManager.persist(this);
        item.setAverageRating(findAverageRatingByItem(item));
        item.merge();
        entityManager.flush();
    }

    @Transactional
    public void remove() {
        Item item;
        if (entityManager == null) entityManager = entityManager();
        if (entityManager.contains(this)) {
            entityManager.remove(this);
            item = this.item;
        } else {
            Review attached = Review.findReview(getId());
            entityManager.remove(attached);
            item = attached.getItem();
        }
        item.setAverageRating(findAverageRatingByItem(item));
        item.merge();
        entityManager.flush();
    }

    @Transactional
    public it.unibz.sipai.domain.Review merge() {
        if (entityManager == null) entityManager = entityManager();
        Review merged = entityManager.merge(this);
        item.setAverageRating(findAverageRatingByItem(item));
        item.merge();
        entityManager.flush();
        return merged;
    }

    @Override
    public int compareTo(it.unibz.sipai.domain.Review o) {
        return Double.compare(o.rating, rating);
    }
    
    //ngocnt.
    //Get the review of items belong to Attraction category
    public static List<Review> findReviewsAttractionCategory() {
        EntityManager em = Review.entityManager();
        TypedQuery<Review> q = em.createQuery("SELECT o "
        		+ "FROM Review AS o "
        		+ "WHERE o.item IN (SELECT i.id FROM Item AS i WHERE i.remoteSystemInfo.remoteSystemName='lcs-poi') "
        		+ "ORDER BY o.creationDate DESC", Review.class);
        return q.getResultList();
    }
}
